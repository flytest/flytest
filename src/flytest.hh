#include <stdio.h>
#include <vector>
#include <map>
#include <string>
#include <cassert>
#include <cstdlib>
#include <typeinfo>
#include <iostream>

#define _FLYTEST_TOKENPASTE_IMPL(x, y) x ## y
#define _FLYTEST_TOKENPASTE(x, y) _FLYTEST_TOKENPASTE_IMPL(x, y)
#define _FLYTEST_UNIQ(UNIQUE) _FLYTEST_TOKENPASTE(_flytest_ ## UNIQUE, __LINE__)

static const int MAX_COUNT = 100;

enum FunctionResult {
    PASSED = 0,
    SKIPPED = 1,
    CANCELLED = 10,
    TIME_OUT = 20,
    FAILED = -1
};

#define promise(EXPR)                               \
    if (!(EXPR)) {                                  \
        this->_result_ = FAILED;                    \
        return;                                     \
    }

#define require(EXPR)                               \
    if (!(EXPR)) {                                  \
        this->_require_failed_();                   \
        return;                                     \
    }

#define flyuntil(EXPR)                              \
    lbl0:                                           \
    this->_in_anything_ = true;                     \
    this->_anything_();                             \
    if (this->m_suite.m_count > MAX_COUNT) {        \
        this->_result_ = TIME_OUT;                  \
        return;                                     \
    }                                               \
    for (int _i_ = 0; _i_ < 3; _i_++)               \
        if ((_i_ == 2) && (!(EXPR))) { goto lbl0; } \
        else if (_i_ == 1) { this->_in_anything_ = false; }   \
        else if (_i_ == 0)



class FlySuite;
class FlyFunction {
public:
    bool _in_anything_;
    FlySuite& m_suite;
    
    FlyFunction(FlySuite& suite) : m_suite(suite) {
    }

    void _anything_();

    void _require_failed_() {
        if (_in_anything_)
            _result_ = CANCELLED;
        else
            _result_ = SKIPPED;
    }

    FunctionResult run() {
        FunctionResult prev = _result_;
        _in_anything_ = false;
        _result_ = PASSED;
        do_run();
        FunctionResult now = _result_;
        _result_ = prev;
        return now;
    }
protected:
    FunctionResult _result_;
    virtual void do_run() = 0;
};

typedef std::map<std::string, FlySuite*> SuiteMap;
SuiteMap& suite_registry() {
    static SuiteMap suite_registry;
    return suite_registry;
}

void print_indent(int indent) {
    for (int i = 0; i < indent; i++) {
        printf("  ");
    }
}

class FlySuite {
    typedef std::pair<const char*, FlyFunction*> FuncInfo;
    typedef std::vector<FuncInfo> FuncMap;
    FuncMap m_starts, m_stops, m_tests;
    int m_indent;
public:
    int m_count;
    FlySuite(const char* name) {
        suite_registry()[name] = this;
    }

    void run_any_test() {
        m_indent += 1;
        m_count++;
        FuncInfo const& test = m_tests[rand() % m_tests.size()];
        print_indent(m_indent);
        printf("%s\n", test.first);
        int test_res = test.second->run();
        if (test_res == SKIPPED) {
            print_indent(m_indent);
            printf("SKIPPED: %s\n", test.first);
        }
        if (test_res == CANCELLED) {
            print_indent(m_indent);
            printf("CANCELLED: %s\n", test.first);
        }
        if (test_res == TIME_OUT) {
            print_indent(m_indent);
            printf("TIME OUT: %s\n", test.first);
        }
        m_indent -= 1;
    }

    void run() {
        m_indent = 0;
        m_count = 0;
        FuncInfo const& start = m_starts[rand() % m_starts.size()];
        print_indent(m_indent);
        printf("%s\n", start.first);
        int start_res = start.second->run();
        assert(start_res == PASSED);

        while (m_count < MAX_COUNT) {
            m_count++;
            FuncInfo const& test = m_tests[rand() % m_tests.size()];
            print_indent(m_indent);
            printf("%s\n", test.first);
            int test_res = test.second->run();
            if (test_res == SKIPPED) {
                print_indent(m_indent);
                printf("SKIPPED: %s\n", test.first);
            }
            if (test_res == CANCELLED) {
                print_indent(m_indent);
                printf("CANCELLED: %s\n", test.first);
            }
            if (test_res == TIME_OUT) {
                print_indent(m_indent);
                printf("TIME OUT: %s\n", test.first);
            }
        }

        FuncInfo const& stop = m_stops[rand() % m_stops.size()];
        print_indent(m_indent);
        printf("%s\n", stop.first);
        int stop_res = stop.second->run();
        assert(start_res == PASSED);
    }

    void register_start(const char* name, FlyFunction* function) {
        m_starts.push_back(FuncInfo(name, function));
    }

    void register_stop(const char* name, FlyFunction* function) {
        m_stops.push_back(FuncInfo(name, function));
    }
    
    void register_test(const char* name, FlyFunction* function) {
        m_tests.push_back(FuncInfo(name, function));
    }
};


void FlyFunction::_anything_() {
    m_suite.run_any_test();
}


#define flysuite(NAME)                                              \
    namespace _FLYTEST_UNIQ(suite) { FlySuite _flysuite_(NAME); }   \
    namespace _FLYTEST_UNIQ(suite)


#define flystart(NAME)                                              \
    struct _FLYTEST_UNIQ(Class) : public FlyFunction {              \
        _FLYTEST_UNIQ(Class)() : FlyFunction(_flysuite_) {          \
            _flysuite_.register_start(NAME, this);                  \
        }                                                           \
        void do_run();                                              \
    };                                                              \
    static _FLYTEST_UNIQ(Class) _FLYTEST_UNIQ(instance);            \
    void _FLYTEST_UNIQ(Class)::do_run()


#define flystop(NAME)                                               \
    struct _FLYTEST_UNIQ(Class) : public FlyFunction {              \
        _FLYTEST_UNIQ(Class)() : FlyFunction(_flysuite_) {          \
            _flysuite_.register_stop(NAME, this);                   \
        }                                                           \
        void do_run();                                              \
    };                                                              \
    static _FLYTEST_UNIQ(Class) _FLYTEST_UNIQ(instance);            \
    void _FLYTEST_UNIQ(Class)::do_run()



#define flytest(NAME)                                               \
    struct _FLYTEST_UNIQ(Class) : public FlyFunction {              \
        _FLYTEST_UNIQ(Class)() : FlyFunction(_flysuite_) {          \
            _flysuite_.register_test(NAME, this);                   \
        }                                                           \
        void do_run();                                              \
    };                                                              \
    static _FLYTEST_UNIQ(Class) _FLYTEST_UNIQ(instance);            \
    void _FLYTEST_UNIQ(Class)::do_run()
