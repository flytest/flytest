#include "flytest.hh"
#include <time.h>

class Stack {
    std::vector<int> m_elems;
public:
    void push(int elem) {
        m_elems.push_back(elem);
    }

    int pop() {
        int elem = top();
        m_elems.pop_back();
        return elem;
    }

    int top() const {
        return m_elems[m_elems.size() - 1];
    }

    size_t size() {
        return m_elems.size();
    }

    void print() {
        printf("[");
        for (int i = 0; i < m_elems.size(); i++) {
            printf("%d, ", m_elems[i]);
        }
        printf("]\n");
    }
};




flysuite("Stack") {
    
    Stack stk;
    int counter = 0;

    flystart("create a stack") {
        stk = Stack();
    }


    flystop("destroy a stack") {
        //delete stk;
    }


    flytest("push a value") {
        int before = stk.size();
        stk.push(counter++);
        promise(stk.size() == before + 1);
    }


    flytest("pop is awesome") {
        require(stk.size() > 0);
        int before = stk.size();
        stk.pop();

        promise(stk.size() == before - 1);
    }


    flytest("push then pop is cool") {
        int before = stk.size();
        int value0 = counter++;
        stk.push(value0);

        flyuntil(stk.top() == value0) {
            require(stk.size() > before);
        }

        int value1 = stk.pop();
        promise(value0 == value1);
        promise(stk.size() == before);
    }

}


int main() {
    int t = time(NULL);
//t=1410985087;
    printf("t=%d\n", t);
    srand(t);
    SuiteMap const& registry = suite_registry();
    for (SuiteMap::const_iterator it = registry.begin(); it != registry.end(); it++) {
        it->second->run();
    }
}   